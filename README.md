# Introduction:

DNA or RNA motifs are short (5-20 bp) recurring patterns that are presumed to have a biological function by binding to proteins. Searching for these small patterns in large genomic data (up to billions bp) is very challenging. These To address this, we have built a consolidated pipeline called the `Evolutionay Motif Scan` (EMSCAN). EMSCAN presents a hierarchy of increasingly sophisticated motif scanning algorithms that are considered, testing their ability to identify known binding sites in a genomic sequence. The EMSCAN pipeline currently consists of multiple evolutionary methods to mimic the evolution of binding motifs and can utilise phylogentic relationship from orthologous sequences to predict biologically relevant motifs.
Further we are implementing a deep learning framework to predict most probable combination of two of more such motifs from various permutations possible in a given sequence.

<img src="./data/deep-emscan.png">


# Usage Details:

### Inputs needed
- Multi sequence alignments
- A PWM matrix (DNA motif)
- [Optionally] A set of unaligned sequences known to contain a motif to derive a PWM from.
- Phylogenetic tree

### Output
- Matching positions and scores associated as the motif windows moves on the sequence
- Evolutionary model that best defines the motif evolution
- Conservation pattern of the motif in orthologous sequences
- Combination of motifs

## Usage syntax for emscan (Perl)

```
$PGM="src/bin/emscan"               # usage message
  USAGE:
        $PGM [-s <s>] [-tid <tid>] [-tfile <tfile>] [-adir <adir>]
          [-bkg <bkg>]
          [-tree <tree> -lambda <lambda> -lambda0 <lambda0>
           -m_sub_model <Fix>|JC> -b_sub_model <MM0|JC>]


        [-s <s>]                scoring function to use; default: $s
        [-tid <tid>]            TRANSFAC id of matrix to score with;
                                default: $tid
        [-tfile <tfile>]        name of TRANSFAC count matrix file;
                                default: $tfile
        [-adir <adir>]          directory containing alignment files;
                                default: $adir
        [-bkg <bkg>]            file of base frequencies in
                                fasta-get-markov format;
                                default: $bkg
        [-tree <tree> -lambda <lambda> -lambda0 <lambda0>
           -m_sub_model <Fix>|JC> -b_sub_model JC|MM0]
                                required for score functions 4, 5, 6, 7

        Score each alignment in the files given in the input:

                [<alignment file> <strand to use> <downstream ORF>]+

        The matrix given in <tfile> and scoring function <s> are
        used to compute the score.

 	Reads standard input.
        Writes standard output.

Copyright Reserved (2020) Monash University

```
## Usage syntax for emscan (Python)

### Simple scoring
```
python simpleScoring.py -p ../../data/MA0139.1.jaspar -s ../../data/iYAL005C_YAL003W.aln.fasta -f jaspar

-p option (The filepath option for the SPCM matrix which will be converted to PWM)

-f options (Format option for the type of input for the PSCM matrix)
transfac # For transfac format file input
jaspar # For jaspar format file input

-s option (The filepath option of the gapless sequence blocks)

```

### Evolutionary scoring models
```
cd src/python
python cal_scores.py -p ../../data/transfac_matrix_1.txt -f transfac -s ../../data/gapless-msa-blocks -m jc (For JC model)

-p option (The filepath option for the SPCM matrix which will be converted to PWM)

-f options (Format option for the type of input for the PSCM matrix)
transfac # For transfac format file input
jaspar # For jaspar format file input

-s option (The filepath option of the gapless sequence blocks)

-m options (Evolutionary model selection)
jc
fix
hky
```

## Authors

- Prof Timothy Bailey, University of Queensland
- Dr Sonika Tyagi, Monash University
- Mr Tarun Bonu, Monash University
