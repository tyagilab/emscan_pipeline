import subprocess
import pandas as pd
import numpy as np

def get_shape_score(shape_file, start_pos):

    try:
        start_pos = str(start_pos)
        score_series = subprocess.check_output(["grep", start_pos, "-A", '30' , shape_file])
        score = sum([float(item.split(' ')[1]) for item in str(score_series).split('\\n')[:-1]])
        return score
    except:
        return np.nan

training_data = pd.read_csv('../data/training_data.csv')

shapes = ['Shift']

for shape in shapes:
    shape_scores = []

    for ix, row in training_data.iterrows():
        motif1_len = len(row['bs1'].split('_')[1])
        motif_mid = int(np.ceil(motif1_len/2))
        DNA_start = int(row['bs1_start_pos']) + motif_mid - 15
        chromosome = row['chromosome']

        shape_file = '../data/DNA_shapes/hg38.' + shape + '/hg38.' + shape + '.' + chromosome + '.wig'
        score = get_shape_score(shape_file, DNA_start)
        shape_scores.append(score)

        if ix % 200 == 0:
            with open('./log_file_shift.txt', 'a') as fp:
                fp.write(shape + ' -- row number--> ' + str(ix) + '-- Score -->' + str(score) + ' --- Percentage completed --> ' + str((ix/training_data.shape[0]) * 100) + '\n')
        
#        if not ix:
#            break

#    print(shape, shape_scores)
#    break

    training_data[shape + '_score'] = shape_scores
    training_data.to_csv('../data/training_data_with_shift_scores.csv', index = False)
