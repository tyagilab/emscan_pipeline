=head1 NAME   ScoringMAtrix.pm
Class for Position Specific Scoring Matrix (PSSM)
  PSSM is normally calculated from a position
  specific freq. matrix.
  Each cell of the PSSM is filled by log-odds ratio of
  the freq. of a letter at that position.
=cut



#code begins here
package ScoringMatrix;

use vars '@ISA';
use PSFM;
@ISA = qw(PSFM); # this class inherits methods from PSFM.pm

=head2 new
Title   : new
Usage   : my $matrix = ScoringMatrix->new(%args);
Function: Constructor for the PSSM object
Returns : a new ScoringMatrix object.
Args    : a) Following arguments must be specified

=cut

sub new  {

 my $class = shift;
 my %args = @_;
 # first figure out if matrix has been provided

 if (defined $args{'-tfacfile'}) {
     $matrix= PSFM->get_matrix(%args); # matrix object of type PSFM
 } else  {
    print STDERR "No matrix provided\n";
 }

 my $self = bless $matrix, ref($class) || $class;

 if($args{-pssm}) {
   $self->{pssm} = $args{-pssm};
 } else {
   $self->{pssm} = $self->_get_pssm();
 }

 #for debuggin: print pssm and exit
 if(defined $args{-print_pssm} and $args{-print_pssm}==1 ) {
          foreach (@{$self->{pssm}}) {
                  foreach(@{$_}) {
                  printf (STDERR "%5.2f ", $_);
                  }
          print STDERR   "\n";
          }
  } # print_pssm

 return $self;
}



sub PSSM{
    $self = shift;
    return $self->{pssm};
 }

sub rev_comp_PSSM{

   $self = shift;
    my @A = @{$self->{pssm}->[3]}; #T is complementry of A
    my @C = @{$self->{pssm}->[2]};#
    my @G = @{$self->{pssm}->[1]};
    my @T = @{$self->{pssm}->[0]};
    #print "A=@A\n";
    @A=reverse @A; @C=reverse @C;@G=reverse @G;@T=reverse @T;
    # reverse the freq
    #print "reverse A =@A\n";
    my @revcomp = (\@A,\@C,\@G,\@T);
    my $rev = \@revcomp;

    return $rev;
    #return ScoringMatrix->new('-pssm' => $rev);;
}

=head2 _get_pssm
Title   : _get_pssm
Function: Get the Position specific scoring matrix
Returns : ref to an array contianing probabilities of A,G,C,T
Args    : none
=cut

sub _get_pssm{
  $self = shift;
  $psfm = $self->get_freq_matrix;
  my @ntd = qw(A C G T);
  my @weigths;
  for(my $i=0; $i<=$#ntd; $i++) {
      $bkg_freq = $self->{bg_freqs}; # Returns a ptr to a hash keyed in Ntd.
      my @freq = ();
      my $j = 0;
      foreach my $f (@{$psfm->[$i]}) {
	$freq[$j] = (log($f/$bkg_freq->{$ntd[$i]}))/log(2);
	#printf ("%.3f\t",$wt[$j]);
        $j++;
      } #foreach f
  push(@weights,\@freq);
  #print "\n";
  }
  return \@weights;
}




sub DESTROY
{    # nothing
}
1;
