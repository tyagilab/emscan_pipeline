=head1 NAME   PSFM.pm
This class creates a PSFM matrix object
INPUT:
    1) File containing TRANSFAC count matrices.
    2) Transfac ID or Accession no.
    3) Background frequencies of A,C,G and T
RETURNS:
    PSFM object

=cut

package PSFM;

sub new {
  $class = shift;
  my %args = @_;  
  my $self = bless {}, $class;    

 # Set the object data.
  $self->{'bg_freqs'} = ($args{'-bg_freqs'} || [0.25, 0.25, 0.25, 0.25]);
  $self->{'freq_matrix'} = $args{-freq_matrix};
  $self->{'ID'} = ($args{-ID} or
                   $self->{ID} or
                         "Unknown");
  $self->{'AC'} = ($args{-AC} or
                   $self->{AC} or
                      "Unknown");
  $self->{'species'} = ($args{-species} or
                   $self->{species} or
                      "Unknown");
  $self->{'desc'} = ($args{-desc} or
                   $self->{desc} or
                      "Unknown");

 return $self;
}

#
# read a TRANSFAC file and get a PSFM object
#
sub get_matrix{   
  my $class = shift;   
  my %args = @_;  

  if (defined $args{-bkgfile}) {	# background file name
    open(BKG, $args{-bkgfile}) or die "Could not open $args{-bkgfile}: $!";
    @Bkg=<BKG>;				# read fasta_get_markov file	
    close(BKG);
    $bg_freqs = _get_bkg(\@Bkg);	# get array of frequencies
  } #

  if (defined $args{'-pseudocount'}) {
     $pseudocount = $args{'-pseudocount'};
  } else {
     $pseudocount = 1;
  } 

  if (defined $args{'-tfacfile'}) {
    open(MAT,$args{'-tfacfile'}) || die "Cannot open file $args{'-tfacfile'}: $!.";
    @filedata=<MAT>;
    close(MAT);
    if(defined $args{'-ID'}) {
      ($matrix,$ID,$AC,$sps,$desc) =_mat_by_ID($args{'-ID'},\@filedata,$pseudocount,$bg_freqs);
    } elsif(defined $args{'-AC'}) {
      ($matrix,$ID,$AC,$sps,$desc) =_mat_by_ACCN($args{'-AC'},\@filedata,$pseudocount,$bg_freqs);
    } else { 
      print STDERR "Please specify the ID or Accession number of the TRANSFAC matrix\n";
      exit(1);
    }
  } else {
     print STDERR "Please provide the TRANSFAC matrices file\n";
     exit(1);
  }

  $matrix_obj = PSFM->new(
    '-bg_freqs'    => $bg_freqs,			# ref. to array of frequencies
    '-freq_matrix' => $matrix,				# ref. to matrix[col][b]
    '-ID'          => $ID,
    '-AC'          => $AC,
    '-species'     => $sps,
    '-desc'        => $desc
  );    

  return $matrix_obj;
} # get_matrix 


################### Accessor methods############## 

=head2 
Title   : rev_comp_PSFM
Function: get freq. matrix for the reverse complementry strand
Returns : ref. to an array of position sp. wts.
          in the order A,C,G,T
Args    : None.
=cut
sub rev_comp_PSFM {
  my $self = shift;
  
  my $ncols = $self->get_len_motif();
  my $i;

  # reverse complement each column
  my @revcomp;
  my $old_matrix = $self->get_freq_matrix;
  for ($i=0; $i<$ncols; $i++) {		# column
    my $old_col = $old_matrix->[$i];
    my $col;
    # swap A and T
    $col[0] = $old_col->[3];	
    $col[3] = $old_col->[0];
    # swap C and G
    $col[1] = $old_col->[2];	
    $col[2] = $old_col->[1];
    $revcomp[$ncols-1-$i] = [@col];
  }

  return PSFM->new('-freq_matrix' => \@revcomp);
} # rev_comp_PSFM

=head2
Title   : get_len_motif
Function: get the length of the motif represented by PSFM
Returns : an integer value.
Args    : none.
=cut
sub get_len_motif{
  my $self=shift;
  my $freq_matrix = $self->get_freq_matrix;
  return($#$freq_matrix + 1);
}


=head2 ID
Title   : ID
Usage   : my $ID = $psfm->ID()
	 $psfm->ID('M00049');
Function: Get/set on the ID of the Transfac matrix
Returns : pattern ID (a string)
Args    : none for get, string for set
=cut
sub ID {
  my ($self, $ID) = @_;
  $self->{'ID'} = $ID if $ID;
  return $self->{'ID'};
}

sub get_bg_freqs {
  my ($self) = @_;
  return $self->{'bg_freqs'};
}

sub AC {
  my ($self, $AC) = @_;
  $self->{'AC'} = $AC if $AC;
  return $self->{'AC'};
}
 
sub desc {
  my ($self, $name) = @_;
  $self->{'desc'} = $name if $name;
  return $self->{'desc'};
}

sub species {
  my ($self, $sps) = @_;
  $self->{'species'} = $sps if $sps;
  return $self->{'species'};
}

sub get_freq_matrix {
  $self = shift;
  return $self->{freq_matrix};
}

sub print {
  my $self = shift;

  my $i = 1;
  foreach (@{$self->get_freq_matrix}) {
    print "col ", $i++, ": ";
    foreach (@{$_}) {
      printf (STDERR "%.3f\t", $_);
    }
    print STDERR   "\n";
  }
} # print


################### private methods############## 
=head2 
 Title   : _mat_by_ACCN 
 Returns : matrix coresponding to the accession no provided
 Args    : Transfac accession no. of the matrix,
           file containing TRANSFAC matrices,
           Pseudocount and Background feqs.
=cut

sub _mat_by_ACCN  {
  my ($accn,$filedata,$pseudocount,$bkg) = @_;

  my ($datablock) = _read_one_block ($accn,0,$filedata);
  ($pscm,$id,$ac,$sps,$desc) = _get_PSCM($datablock);
  $psfm = to_PSFM($pscm,$pseudocount,$bkg);
  return($psfm,$id,$ac,$sps,$desc); 
}

=head2 
 Title   : _mat_by_ID
 Returns : matrix coresponding to the TF-ID  provided
 Args    : Transfac ID of the matrix,
           file containing TRANSFAC matrices,
           Pseudocount and
           Background feqs.
=cut
sub _mat_by_ID  {
  my ($ID,$filedata,$pseudocount,$bkg) = @_;
  my ($datablock) = _read_one_block($ID,1,$filedata);
  ($pscm,$id,$ac,$sps,$desc) = _get_PSCM($datablock); 
  $psfm = to_PSFM($pscm,$pseudocount,$bkg);
  return($psfm,$id,$ac,$sps,$desc); 
}

sub _read_one_block {
  ($tag,$num,$matfile) = @_;
  my ($ID,$acc)="";
  undef $ID;
  undef $acc;
  if($num ==0) {
    $acc =$tag;
  } elsif($num ==1) {
    $ID =$tag;
  } else {
    print "Arguments not passed correctly"; exit(1);
  }
  my @block = ();
  my $hit = 0;
  foreach my $line (@$matfile) {
    if ($line eq "//\n") {
      foreach my $lineinblock (@block) {
	if (defined $ID) {
	  if ($lineinblock eq "ID  $ID\n") { $hit = 1 };
	}
	if (defined $acc) {
	  if ($lineinblock eq "AC  $acc\n") { $hit = 1 };
	}
      } # foreach lineblock
      if ($hit == 0) { @block = (); }
    } #if finds '//'
    if ($hit == 0) { push @block, $line; }
  } # foreach line
  return (\@block);
}

=head2 
Title   : _get_PSCM
Function: Get a count matrix from a TRANSFAC entry.
Returns : 
        \@matrix ref. to count array[col][base]
	$ID 
        $AC
        $species
        $desc
=cut
sub _get_PSCM() { 
  my (
    $datablock					# TRANSFAC entry
  ) = @_;

  my @datalines = @$datablock;
  my (@matrix) = ();
  my ($desc, $ID, $AC) = "";

  foreach my $line (@datalines) {		# line in entry
    if ($line =~ /^((\s)?BF)\s+(\S+).*\n/) {
      $desc = $3;
      $line =~ /Species:\s+(\S+)\s+(\S+)\s+(\S+)/;
      $species = $2." ".$3; 
    }
    if ($line =~ /ID\s+(\S+)$/) {
      $ID = $1;
    }
    if ($line =~ /AC\s+(\S+)$/) {
      $AC= $1; 
    }
    # parse line of counts
    if ($line =~ /^\d{2}\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+).*$/) {
      push @matrix, [$1, $2, $3, $4];
      #print STDERR "$1 $2 $3 $4\n";
    }
  }

  return (\@matrix, $ID, $AC, $species, $desc);
} # get_PSCM

=head2 
Title   : to_PSFM
Function: convert a count matrix into freq matrix
	  Adds c*bkg_b to count for base b.
Returns : 
		\@freqs[col][base]
=cut
sub to_PSFM { 
  my(
    $pscm,		# count matrix
    $c,			# total pseudocounts
    $bkg		# ref. to pseudocount freq vector
  ) = @_; 

  my @freqs;		# return value

  foreach my $c_col (@$pscm) {	# count column
    my @f_col; 				# frequency column
    # get sum of counts in column
    my $col_sum = 0;
    my $alen = $#$c_col + 1;
    for ($i=0; $i<$alen; $i++) { $col_sum += $c_col->[$i]; }
    # compute
    for($i=0; $i<$alen; $i++) { 	# letter
      $f_col[$i] = ($c_col->[$i] + ($bkg->[$i] * $c))/($col_sum + $c);
    } # letter
    #for ($i=0; $i<$alen; $i++) { print STDERR "$f_col[$i] : ";} print STDERR "\n";
    push(@freqs, \@f_col);
  }
  return \@freqs;
} # to_PSFM

=head2 _get_bkg 
Title   : _get_bkg
Function: Get the background probabilities from an array 
		containing lines in fasta_get_markov format
Returns : ref. to array of frequencies in order ACGT
Args    : filedata (an array reference)
=cut
sub _get_bkg {
  ($data) = shift;

  my ($letter, $freq);  
  my %hash=();

  foreach (@$data) {
    if(/^[ACGT]\s/) {
      ($letter, $freq) = split;
      $hash{$letter} = $freq;
    } #if acgt
  } #foreach data

  return [$hash{'A'}, $hash{'C'}, $hash{'G'}, $hash{'T'}];
} # _get_bkg

sub DESTROY  
{    # nothing
}
1;
