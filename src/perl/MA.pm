package MA;

=head2 new
 Title   : new
 Usage   : my $block  = new(%args);
 Function: Creates a multiple alignment object
 Returns : a multiple alignment object
 Args    : %args =( 
		  -sr_no => block_sr_no,
		  -width => block_width,
		  -seqs => ptr_to_list_of_seqs, each seq. is a string
		  -ids => ptr_to_list_of_ids,
		  -offsets => ptr_to_list_of_offsets,
                  )
  Attributes:
	alphabet: array of strings
	array:	list of matrix columns as strings
	row_num: dictionary from id to row number in alignment
=cut

sub new {
  my $class = shift;
  my %args = @_;
  my $self = bless {}, $class;

  #set the values to object attributes
  if(defined $args{-sr_no}) {
    $self->{'sr_no'} = $args{-sr_no} ||  $self->{'sr_no'};
  }
  if(defined $args{-width}) {
    $self->{'width'} = $args{-width} ||  $self->{'width'};
  }
  if(defined $args{-seqs}) {
    $self->{'seqs'} = $args{-seqs} ||  $self->{'seqs'};
    $self->{'array'} = $self->make_ma_array;
  }
  if(defined $args{-ids}) {
    $self->{'ids'} = $args{-ids} ||  $self->{'ids'};
    # make dictionary for looking up row number of id
    my $ids = $self->{'ids'};
    my $i;
    my %row_num = ();
    for ($i=0; $i<=$#$ids; $i++) {
      $row_num{$ids->[$i]} = $i;
    }
    $self->{'row_num'} = \%row_num;
  }
  if(defined $args{-offsets}) {
    $self->{'offsets'} = $args{-offsets} ||  $self->{'offsets'};
  }
  if(defined $args{-offsets}) {
    $self->{'alphabet'} = $args{-alphabet} ||  $self->{'alphabet'};
  }
  return $self;
} # new

#
#  make_ma_array
#
#  Create an array containing the MA columns.
#  Each column is stored as a string.
#
sub make_ma_array {
  my($self) = @_;

  my $seqs = $self->get_seqs();
  my $len = length($seqs->[0]);
  my @array;

  for (my $i=0; $i<$len; $i++) {
    my $col;
    foreach $seq (@$seqs) {
      $col .= substr($seq, $i, 1);
    }
    push @array, $col;
  }
  return(\@array); 
} #  make_ma_array

=head2
  Title   : read_gapless_file
  Function: reads in gapless block data 
  Returns : (pointer to) an array of objects of type MA class. 
  Args    : -sfile <sequence_file_name>
  Note	  : "." is transliterated to "-"

=cut
sub read_gapless_file {
  ($self, %args) = @_;
  my (@blk_width,@seqid,@sequence,@offsets,@blocks,@array)=();

  my @alphabet = qw(A C G T);		# DNA alphabet

  # check input arguments
  if (defined $args{-sfile}) {
    open(FILE, "$args{-sfile}") || die "Cannot open file $args{-sfile}: $!\n";
  } else { 
    die "Please specify sequence/MA file!\n";
  }

  my $flag =0;				# not in a block yet
  my $srno = 0;
  my ($pos,$jnk);
  my $cur_sequence;
  undef $cur_sequence;
  #
  # read the file and create an MA object for each gapless block read
  #
  while(<FILE>) {
    chomp;
    if(/^#GAPLESS_BLOCK/) { 
      if (defined $cur_sequence) {	# store previous sequence
	push(@sequence, $cur_sequence);
	undef $cur_sequence;
      }
      if ($flag) {			# create MA object for previous block
        my $ma_obj = MA->new(
			     '-sr_no' => $srno,
			     '-width' => $blk_width[$srno],
			     '-ids' => [@seqid],
			     '-seqs' => [@sequence],
			     '-offsets' => [@offsets],
			     '-alphabet' => [@alphabet]
			      );
        push(@blocks, $ma_obj);		# add block to array of blocks to return
	undef @seqid; 			# no current set of ids
        undef @offsets; 		# no current set of offsets
        undef @sequence;		# no current set of seqs
        $srno++;			# serial number of next block
      } # flag
      $flag = 1;			# flag that we're in a block
      /BLOCK_WIDTH\s+(\d+)/g;		# get block width
      $blk_width[$srno] = $1;		# record width of current block
    } elsif(/>(\S*)/) {
      if (defined $cur_sequence) {	# store previous sequence
	push(@sequence, $cur_sequence);
	undef $cur_sequence;
      }
      push(@seqid, $1);			# store current sequence id
    } elsif(/^#SEQ_Start_offset\s+(\S+)/) {
      push(@offsets, $1);
    } elsif($_ !~ /#/ and $_ !~ /^\s*$/) {	# not blank or comment
      tr/./-/;
      $cur_sequence .= $_;		# add to current sequence
    }
	       
  } # while
  close(FILE);

  # 
  # Finish the last sequence and block
  #
  if (defined $cur_sequence) {	# store previous sequence
    push(@sequence, $cur_sequence);
  }
  if ($flag) {				# create MA object for last block
    $ma_obj = MA->new(
		     '-sr_no' => $srno,
		     '-width' => $blk_width[$srno],
		     '-ids' => [@seqid],
		     '-seqs' => [@sequence],
		     '-offsets' => [@offsets],
		     '-alphabet' => [@alphabet]
		     );
    push(@blocks, $ma_obj);		# add block to list
  }

  #
  # return (reference to) list of MA objects
  #
  return \@blocks;			
} # read_gapless_file


=head2
  Title   : ids 
  Function: gets the id information for each gapless block
  Returns : returns a ref. to an array of strings.
  Args    : None
=cut
sub get_ids {
  $self = shift;
  return $self->{ids};
} # get_ids

=head2
  Title   : get_row_index 
  Function: gets the row index for a given 'seq_name'
  Returns : returns an integer.
  Args    : id; a string
=cut
sub get_row_index {
  $self = shift;
  my $id = shift;
  return($self->{'row_num'}->{$id});
} # get_row_index

=head2
  Title   : get_id_row_dict
  Function: gets the id => row dictionary
  Returns : ref. to a dictionary
  Args    : 
=cut
sub get_id_row_dict {
  $self = shift;
  return($self->{'row_num'});
} # get_id_row_dict

=head2
  Title   : get_seqs
  Function: get the block seqs. 
  Returns : returns a ref. to an array of strings.
  Args    : None
=cut
sub get_seqs {
  $self = shift;
  return $self->{seqs};
} # get_seqs


=head2
  Title   : get_array
  Function: get sequence array
  Returns : returns a ref. to an array of strings, each string is a column.
  Args    : None
=cut
sub get_array {
  $self = shift;
  return $self->{array};
} # get_array


=head2
  Title   : get_offsets
  Function: get the start offset of blocks
  Returns : returns a ref. to an array of integers.
  Args    : None
=cut
sub get_offsets {
  $self = shift;
  return $self->{offsets};
} # get_offsets


=head2
  Title   : get_width 
  Function: get the width of block
  Returns : returns an integer.
  Args    : None
=cut
sub get_width {
  $self = shift;
  return $self->{'width'};
} # get_width


=head2
  Title   : get_sr_no
  Function: get the serial no.s of block
  Returns : returns a ref. to an array of integers.
  Args    : None
=cut
sub get_sr_no {
  $self = shift;
  return $self->{sr_no};
} # get_sr_no

=head2
  Title   : get_col
  Function: extracts an alignt. col. at a given position 'i' of MA
  Returns : returns a string
  Args    : value of 'i', an integer from [0...n], where n is 
            length of the alignment
=cut
sub get_col {
  my ($self, $i) = @_;

  return($self->get_array->[$i]);
} # get_col

sub get_alphabet {
  my ($self) = @_;

  return($self->{alphabet});
} # get_alphabet

=head2
  Title   : print
  Function: print an MA object
  Returns :
  Args    :
=cut
sub print {
  my $self = shift; # calling obj.
  my $seqs = $self->get_seqs();
  my $array = $self->get_array();
  my $ids = $self->get_ids();
  my $i;
  print "alphabet:"; foreach my $a (@{$self->get_alphabet}) { print " $a"; }; print "\n";
  for ($i=0; $i<=$#$seqs; $i++) {
    print $ids->[$i], " ", $seqs->[$i], "\n"; 
  }
  my $row_dict = $self->get_id_row_dict;
  foreach $key (keys %$row_dict) {
    printf "id $key row %d\n", $row_dict->{$key};
  }
  #for ($i=0; $i<=$#$array; $i++) {
  #  print $i, " ", $array->[$i], "\n"; 
  #}
} # print

sub DESTROY  {
    # nothing
}
1;
