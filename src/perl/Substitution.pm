=head1 NAME Substitution.pm
	Base substitution models.
=cut

=head2
Define functions here with names prob_b_given_a_X.
Invoke them via a function pointer returned by get_subst_model as:

Name      : prob_b_given_a
Usage     : $prob = prob_b_given_a(args);
Function  : calculates prob. of substituting an ancestral base 'a' by
            base 'b' over the branch length 't'.
Arguments : base 'b'
            base 'a'
            branch_length 't'
            freq. of b 'f'
            value_of_lambda 'lambda'
Returns   : probability of 'b' given 'a'.
=cut

#
# Ignores the evolutionary distances and everything else except f_b:
# Pr(b | a, lambda, t) = f_b
#
# Use this for computing non-evolutionary alignment probabilities.
#
sub prob_b_given_a_none {
  my(
   $b,         # modern base 'b'
   $a,         # ancestral base 'a'
   $f_b,       # frequency of base 'b' at the current position
   $lambda,    # neutral mutation rate
   $mu,        # probability of mutation
   $t          # tree branch length
  ) = @_;
  return($f_b);
} # prob_b_given_a_none

=head2
Fixation model of base substitution:
  The fixation model can be used to model base substitutions under a motif model.
  It assumes that the ancestral TF had the same base affinities as the modern TFs 
  and, consequently, that the ancestral TFBS had the same base distributions
  as the modern ones.
  Under the fixation model, the probability of base 'b' replacing base 'a'
  over branch length 't' depends on the motif emission frequencies:
      Pr_FIX(b|a, f, t, lambda) = mu f_b + (1 - mu)delta_ab,
  where mu = 1 - e^{-lambda t}, and f = M_i is the emission vector for
  site 'i' in the motif.
=cut
sub prob_b_given_a_fixation {
  my(
   $b,         # modern base 'b'
   $a,         # ancestral base 'a'
   $f_b,       # frequency of base 'b' at the current position
   $lambda,    # neutral mutation rate
   $mu,        # probability of mutation
   $t          # tree branch length
  ) = @_;
  return(($mu * $f_b) + (($a eq $b) ? (1 - $mu) : 0));
} # prob_b_given_a_fixation

=head2
The Jukes-Cantor model assumes equal rates of mutation between
all possible pairs of bases, 'a' and 'b', where 'a,b in alphabet.
Under this model ('JK'), the probability of base 'b' replacing base 'a'
over branch length 't' is
   Pr_JK(a|b,f,T,lambda) = 1/4 + (3/4 e^{4/3}lambda* t )delta_ab
        + (1/4) - ( (1/4)e^{(4/3)lambda * t} ) (1-delta_ab).
This model can be used for computing the likelihood under motif and
background models.  (The value of 'f' is ignored by this model, but
is included for completeness.
=cut
sub prob_b_given_a_JK {
  my(
   $b,         # modern base 'b'
   $a,         # ancestral base 'a'
   $f,         # frequency of base 'b' at the current position
   $lambda,    # neutral mutation rate
   $mu,        # probability of mutation
   $t          # tree branch length
  ) = @_;

  my $p_jk;
  if($a eq  $b) {
    $p_jk = (1/4) + ((3/4) * exp( (4/3) * -($lambda * $t)));
  } else {
    $p_jk = (1/4) - ((1/4) * exp( (4/3) * -($lambda * $t)));
  }
  return $p_jk;
} # prob_b_given_a_JK 

#
# get_subst_model
#   Returns a pointer to a substitution model function
#   corresponding to the given model name.
#   Supported models:
#	Fix	Tamura-Nei with \alpha_R = \alpha_Y = 1, equil. freq. = motif freq., f_b 
#	JC	Jukes-Cantor
#	None	f_(b|a,t,\lambda) = f_b 
#
sub get_subst_model {
  (
    $subst_model_name			# name string
  ) = @_;
  my $subst_model;

  if ($subst_model_name eq "None") {
    $subst_model = \&prob_b_given_a_none; 
  } elsif ($subst_model_name eq "JC") {
    $subst_model = \&prob_b_given_a_JK; 
  } elsif ($subst_model_name eq "Fix") {
    $subst_model = \&prob_b_given_a_fixation;
  } else {
    print "Substitution model $subst_model_name not recognized.\n";
    exit(1);
  } #if subs. model

  return($subst_model);

} # get substition model

1;
