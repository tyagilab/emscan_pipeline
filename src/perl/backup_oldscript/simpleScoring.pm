=head1 NAME
  Class: simpleScoring.
    Searches sites with a PSSM and a given scoring function.
    Calculates scores using scoring function 1, 2 and 3.
    Inputs are matrix object returned by TFAC_ScoringMatrix and gapless
    block data by ReadFasta.
    prints out the scores on standard output.
=cut

#code begins here
package simpleScoring;

use ScoringMatrix;
use MA;


sub pattern_search{
  my $class = shift;
  my %args = @_;

  # create the PSSM from TRANSFAC count matrix and background model
  my $matrix = ScoringMatrix->new(%args); # matrix object of type ScoringMatrix
  my $i;
  my $sname = $args{"-sname"};				# name to print for each score

  # read in the multiple alignments
  my $ma_list = MA->read_gapless_file(%args);		# ref to array of MA objects

  my $mat_id = $matrix->ID;
  my $mat_ac = $matrix->AC;
  my $mat_sps = $matrix->species;
  print STDOUT "# Hits for $mat_id;$mat_ac from $mat_sps\n\n";

  foreach my $ma (@$ma_list) {				# loop over all multiple alignments
    my $seqs = $ma->get_seqs;				# get sequences from MA
    my $posn = $ma->get_offsets;				# get offsets
    my $nseqs = @$seqs;					# number of sequences
    
    # switch on scoring function
    if($args{-scoring_fn} == 1 || $args{-scoring_fn} == 2 ) { 	# function 1 & 2

      # score first sequence on both strands and print them
      my ($coord, $score_plus, $score_minus) = cal_score($matrix,$seqs->[0], $posn->[0]);
      print_scores($sname, $coord, $score_plus, $score_minus);

    } elsif($args{-scoring_fn} == 3) {			# Score 3
      my $seq_no;
      my $seq0_coord;					# saved coordinates of seq 0
      my @plus_score_list, @minus_score_list;		# lists of references to scores lists
      #
      # Compute all scores for all sequences
      #
      for ($seq_no = 0; $seq_no < $nseqs; $seq_no++) { 	# loop over sequences
        my $seq = $seqs->[$seq_no];			# sequence i
        my $offset = $posn->[$seq_no];			# origin of sequence i
	# score sequence i on both strands
	my ($coord, $score_plus, $score_minus) = cal_score($matrix,$seq, $offset);
        if ($seq_no == 0) { $seq0_coord = $coord; }	# save coord of seq 0
        push @plus_score_list, $score_plus;
        push @minus_score_list, $score_minus;
      } # sequence loop
      #
      # now compute the average score for all points on all sequences
      #
      for ($i = 0; $i < @{$plus_score_list[0]}; $i++) {
        my $avg_score = 0;
        for ($seq_no = 0; $seq_no < $nseqs; $seq_no++) {# loop over sequences
          my $score1 = $plus_score_list[$seq_no]->[$i];	# positive strand score i
          my $score2 = $minus_score_list[$seq_no]->[$i];	# minus strand score i
          $avg_score += ($score1 >= $score2) ? $score1 : $score2;
        } # position in MA
        $avg_score /= $nseqs;				# compute average of score of position i
        printf "%s\t%d\t%s\t%8.3f\n", $sname, $seq0_coord->[$i], "?", $avg_score;
      } # sequence in MA
    } # Score function 3

  } # ma

}# pattern search

######################################################################
# cal_score
#    calculate scores for a pattern match
#       input are sequence, seq_offset.
#       return reference to three array:
#         1) array posn. coord.
#         2) array of scores on positive strand
#         3) array of scores on negative strand
#####################################################################
sub cal_score{

  my(
   $matrix,           # Matrix object
   $SEQ,              # Block seq
   $POSN              # Block_seq offset
   ) =@_;
  my $score1;         # Scores for positive strand
  my $score2;         # Scores for negative strand
  my @sc_pos = ();    # store scores for positive strand
  my @sc_neg = ();    # Store scores for negative starnd
  my @coord = ();     # position coordinates where score for
                      # a pattern was calculated
  my ($i, $j, $base);
  my $width = $matrix->get_len_motif();  # Length of the motif.
  my $pssm_plus = $matrix->PSSM;
  my $pssm_minus = $matrix->rev_comp_PSSM;
  my $lenseq = length($SEQ);
  printf "$lenseq \n";

  for($i=0;$i<=($lenseq-$width+1);$i++) {
    ($score1,$score2) = 0.0;
    my $wdw = substr($SEQ,$i,$width);
#    my @warray = split(//,$wdw);
    for($j=0;$j< length($wdw); $j++) {
      $base = substr($wdw,$j,1);
      if($base eq 'A') {
	$score1 += $pssm_plus->[0]->[$j];
	$score2 += $pssm_minus->[0]->[$j];
      } elsif($base eq 'C') {
	$score1 += $pssm_plus->[1]->[$j];
	$score2 += $pssm_minus->[1]->[$j];
      } elsif($base  eq 'G') {
	$score1 += $pssm_plus->[2]->[$j];
	$score2 += $pssm_minus->[2]->[$j];
      } elsif($base eq 'T') {
	$score1 += $pssm_plus->[3]->[$j];
	$score2 += $pssm_minus->[3]->[$j];
      } elsif($base eq '-') {
	$score1 += $pssm_plus->[4]->[$j];
	$score2 += $pssm_minus->[4]->[$j];
      }
    } # foreach $j
    $coord[$i] = $POSN + $i;
    $sc_pos[$i] = $score1;
    $sc_neg[$i] = $score2;
#print "score1 =$score1, sc2= $score2\n";
  } # foreach $i
  return (\@coord,\@sc_pos, \@sc_neg);

} # cal_scores


#
# Print the maximum of the scores on the positive and negative
# strand for each position.
#
sub print_scores{
  my ($SEQ_ID,$i);
  ($sname,	       # sequence name
  $ref_coord,          # referance to an array of posn coord.
  $ref_s1,             # ref. to an array of scores_pos
  $ref_s2              # ref. to an array of scores_neg
  ) = @_;
  my @sc_pos = @$ref_s1; # dereferance array of scores on + strand
  my @sc_neg = @$ref_s2; # dereferance array of scores on - strand
  my @coord = @$ref_coord;
  my ($score, $strand);
  foreach ($i=0; $i<=$#sc_pos; $i++) {
    if ($sc_pos[$i] >= $sc_neg[$i]) {
      $score = $sc_pos[$i];
      $strand = '+';
    } else {
      $score = $sc_neg[$i];
      $strand = '-';
    }
    printf "%s\t%d\t%s\t%8.3f\n", $sname, $coord[$i], $strand, $score;
  }

} # print_scores


sub DESTROY  {
    # nothing

}
1;
