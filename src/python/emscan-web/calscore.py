# calscore.py

import math
import re
import optparse
import sys

def convert_matrix(matrix, format):
    """
    Read a PWM file with the positional weight matrix (PWM) into positional list
    for easier access of base values based on positions
    """
    if format == 'transfac':
        matrix = re.search(r'(PO[\w\W]*?)XX', matrix)[1] 
        positions = matrix.split('\n')[:-1] 
        positions = [position.split('\t')[1:] for position in positions] 
        return positions
    elif format == 'jaspar':
        matrix = matrix.split('\n')[1:-1] 
        matrix = [line.split() for line in matrix]
        positions = [[None] * 4 for i in range(len(matrix[0]) - 2)] 
        for ix,line in enumerate(matrix):
            jx = 0
            for score in line:
                if score not in ['[', ']']: 
                    positions[jx][ix] = score 
                    jx += 1
        return positions

def add_pseudo_count(matrix, pseudo_count):
    """
    Add given pseudocount value to all the
    values of the matrix
    Returns a matrix with non-zero frequency values
    """
    for ix, row in enumerate(matrix[1:]):
        for jx, value in enumerate(row):
            if value == '0.0':
                matrix[ix + 1][jx] = float(pseudo_count)
            else:
                matrix[ix + 1][jx] = float(value) + pseudo_count
    return matrix

def pfm_to_pwm(pfm):
    """
    Converts PFM to PWM
    Returns a PWM
    """
    pwm = []
    pwm.append(pfm[0])
    for row in pfm[1:]:
        row_sum = sum(row)
        new_row = [value/row_sum for value in row]
        pwm.append(new_row)
    return pwm

def get_formatted_seqs(seqs):
    """
    Format the input of multiple sequences into a standard sequence to
    pass through the EMSCAN pipeline
    Returns a dictionary of Species:Sequence pairs
    """
    seqs = seqs.split('\n')
    all_seqs = {} 
    ix = 0
    inloop = 0
    while(ix < len(seqs)):
        line = seqs[ix]
        seq = ""
        if line.startswith('>'):
            species = line[1:]
            ix += 2 
            line = seqs[ix]
            while((not line.startswith('>')) and (ix+1 < len(seqs))):
                seq += line.lower()
                ix += 1
                line = seqs[ix]
                inloop = 1
        if inloop:
            all_seqs[species] = seq
            ix -= 2 
        ix += 1
        inloop = 0
    return all_seqs

def get_negative_strand(positive_strand):
    """
    Generates a reverse-complimentary sequence of the given sequence
    """
    negative_strand = ""
    for base in positive_strand[::-1]: 
        if base  == 'a':
            negative_strand += 't'
        elif base == 't':
            negative_strand += 'a'
        elif base == 'c':
            negative_strand += 'g'
        elif base == 'g':
            negative_strand += 'c'
        elif base == '-':
            negative_strand += '-'
        else: 
            print('Unexpecteed character in sequence while generating negative strand')
            return 1
    return negative_strand

def calculate_window_score(window_seqs, window_size, motif_matrix, scale_lambda, branch_length, mu, K, model):
    """
    Calculates the window score using respective formulation based on the given model
    Returns window score
    """
    window_score = 0
    base_pos = {'a': 0, 'c': 1, 'g': 2, 't':3} 

    for pos in range(window_size): 
        pos_score = 1
        all_letters = {window_seq[pos] for window_seq in window_seqs} 
        for base1 in all_letters:
            for base2 in all_letters:
                if model == 'jc':
                    pos_score *= prob_b_given_a_JK(base1, base2, scale_lambda, branch_length)
                elif model == 'fix':
                    f_b = float(motif_matrix[pos + 1][base_pos[base2]])
                    pos_score *= prob_b_given_a_fixation(base1, base2, f_b, mu)
                if model == 'hky':
                    pos_score *= prob_b_given_a_HKY(base1, base2, motif_matrix, pos, branch_length, K)
        window_score += pos_score 
    return window_score

def get_evoscore(pos_seqs_dict, motif_matrix, scale_lambda, branch_length, p, q, model):
    """
    Generate evolutionary scores for all the windows in the sequence by sliding a window through
    the whole of the sequence
    Prints the position and the scores associated with windows at those positions
    """
    window_size = len(motif_matrix) - 1 
    mu = scale_lambda * 1 
    K = - math.log((1 - 2*p - q) * math.sqrt(1 - 2*q)) / 2 
    seqs = list(pos_seqs_dict.values()) 
    min_seq_len = min([len(seq) for seq in seqs]) 

    for k in range(min_seq_len - window_size + 1): 
        pos_window_seqs = [seq[k:k+window_size] for species,seq in pos_seqs_dict.items()] 
        neg_window_seqs = [get_negative_strand(pos_seq) for pos_seq in pos_window_seqs] 

        pos_window_score = calculate_window_score(pos_window_seqs, window_size, motif_matrix, scale_lambda, branch_length, mu, K, model) 
        neg_window_score = calculate_window_score(neg_window_seqs, window_size, motif_matrix, scale_lambda, branch_length, mu, K, model) 

        window_score = max(pos_window_score, neg_window_score) 
        print(k, window_score)

def pattern_search(seqs, motif_matrix):
    seqs = seqs.split('\n')[:-1]
    window_size = len(motif_matrix) - 2

    for ix in range(0, len(seqs)-1, 2):
        species = seqs[ix]
        positive_base_seq = seqs[ix + 1]
        positive_base_seq = positive_base_seq.lower()
        negative_base_seq = get_negative_strand(positive_base_seq)

        scores = []

        for start_pos in range(0, len(positive_base_seq) - window_size):
            end_pos = start_pos + window_size + 1
            pos_sub_seq = positive_base_seq[start_pos: end_pos]
            neg_sub_seq = negative_base_seq[start_pos: end_pos]

            if ('-' in pos_sub_seq) and ('-' not in neg_sub_seq):
                neg_score = cal_score(neg_sub_seq, motif_matrix)
                pos_score = 0
            if ('-' in neg_sub_seq) and ('-' not in pos_sub_seq):
                pos_score = cal_score(pos_sub_seq, motif_matrix)
                neg_score = 0
            if ('-' not in pos_sub_seq) and ('-' not in neg_sub_seq):
                pos_score = cal_score(pos_sub_seq, motif_matrix)
                neg_score = cal_score(neg_sub_seq, motif_matrix)
            else:
                continue

            if pos_score > neg_score:
                scores.append(('+ve', pos_sub_seq, pos_score, start_pos))
                print('+ve', pos_sub_seq, pos_score, start_pos)
            else:
                scores.append(('-ve', neg_sub_seq, neg_score, start_pos))
                print('-ve', neg_sub_seq, neg_score, start_pos)

def cal_score(sub_seq, motif):
    score = 0.0
    for ix, base in enumerate(sub_seq):
        if base == 'a':
            score += float(motif[ix + 1][0])
        elif base == 'c':
            score += float(motif[ix + 1][1])
        elif base == 'g':
            score += float(motif[ix + 1][2])
        elif base == 't':
            score += float(motif[ix + 1][3])
    return score

def prob_b_given_a_none():
    return f_b # type: ignore

def prob_b_given_a_fixation(a, b, f_b, mu):
    if a == b:
        return (mu * f_b) + (1- mu)
    else:
        return (mu * f_b)

def prob_b_given_a_JK(a, b, scale_lambda, branch_length):
    if a == b:
        return (1/4) + ((3/4) * math.exp((4/3) * -(scale_lambda * branch_length)))
    else:
        return (1/4) - ((1/4) * math.exp((4/3) * -(scale_lambda * branch_length)))

def prob_b_given_a_HKY(a, b, motif_matrix, pos, branch_length, K):
    base_pos = {'a': 0, 'c': 1, 'g': 2, 't':3} 

    f_a = float(motif_matrix[pos + 1][base_pos['a']])
    f_c = float(motif_matrix[pos + 1][base_pos['c']])
    f_g = float(motif_matrix[pos + 1][base_pos['g']])
    f_t = float(motif_matrix[pos + 1][base_pos['t']])
    beta = 1 / ((2 * (f_a + f_g) * (f_c + f_t)) + (2 * K * (f_a * f_g + f_c * f_t)))

    if (b in ['a', 'g'] and a in ['c', 't']) or (b in ['c', 't'] and a in ['a', 'g']):
        return(hky_transversion(float(motif_matrix[pos + 1][base_pos[b]]), beta, branch_length))
    else:
        return(hky_transition(a, b, f_a, f_c, f_g, f_t, beta, K, branch_length))

def hky_transition(a, b, f_a, f_c, f_g, f_t, beta, K, branch_length):
    base_pos = {'a': 0, 'c': 1, 'g': 2, 't':3}
    if a == 'a' and a == b:
        term1 = f_a * (f_a + f_g + (f_c + f_t) * math.exp(-beta * branch_length))
        term2 = f_g * math.exp(-(1 + (f_a + f_g) * (K - 1.0)) * beta * branch_length)
        term3 = (f_a + f_g)
        return (term1 + term2)/ term3
    elif a == 'a' and a!=b:
        term1 = f_g * (f_a + f_g + (f_c + f_t) * math.exp(-beta * branch_length))
        term2 = f_g * math.exp(-(1 + (f_a + f_g) * (K - 1.0)) * beta * branch_length)
        term3 = (f_a + f_g)
        return (term1 - term2)/ term3
    elif a == 'g' and a == b:
        term1 = f_g * (f_a + f_g + (f_c + f_t) * math.exp(-beta * branch_length))
        term2 = f_a * math.exp(-(1 + (f_a + f_g) * (K - 1.0)) * beta * branch_length)
        term3 = (f_a + f_g)
        return (term1 + term2)/ term3
    elif a == 'g' and a!= b:
        term1 = f_a * (f_a + f_g + (f_c + f_t) * math.exp(-beta * branch_length))
        term2 = f_a * math.exp(-(1 + (f_a + f_g) * (K - 1.0)) * beta * branch_length)
        term3 = (f_a + f_g)
        return (term1 - term2)/ term3
    elif a == 'c' and a == b:
        term1 = f_c * (f_c + f_t + (f_a + f_g) * math.exp(-beta * branch_length))
        term2 = f_t * math.exp(-(1 + (f_c + f_t) * (K - 1.0)) * beta * branch_length)
        term3 = (f_c + f_t)
        return (term1 + term2)/ term3
    elif a == 'c' and a!= b:
        term1 = f_t * (f_c + f_t + (f_a + f_g) * math.exp(-beta * branch_length))
        term2 = f_t * math.exp(-(1 + (f_c + f_t) * (K - 1.0)) * beta * branch_length)
        term3 = (f_c + f_t)
        return (term1 - term2)/ term3
    elif a == 't' and a == b:
        term1 = f_t * (f_c + f_t + (f_a + f_g) * math.exp(-beta * branch_length))
        term2 = f_c * math.exp(-(1 + (f_c + f_t) * (K - 1.0)) * beta * branch_length)
        term3 = (f_c + f_t)
        return (term1 + term2)/ term3
    elif a == 't' and a!= b:
        term1 = f_c * (f_c + f_t + (f_a + f_g) * math.exp(-beta * branch_length))
        term2 = f_c * math.exp(-(1 + (f_c + f_t) * (K - 1.0)) * beta * branch_length)
        term3 = (f_c + f_t)
        return (term1 - term2)/ term3

def hky_transversion(f_b, beta, branch_length):

    return(f_b * (1.0 - math.exp(-beta * branch_length)))

def calScore(args):
    """
    The main function to be called with options to get the pipeline working.
    This is the entry point to the EMSCAN pipeline
    """

    # The usage of the script with options specified
    parser = optparse.OptionParser(
        usage="python cal_scores.py -t -tf -s -m")

    # Parsing through the optional inputs provided on the command line
    parser.add_option('-p', '--pscm', action='store',
                        dest='pscm', help='The filepath for the motif positional matrix')
    parser.add_option('-f', '--format', action='store',
                        dest='format', help='The format of the motif positional matrix, \'transfac\' or \'jaspar\'')
    parser.add_option('-s', '--sfile', action='store',
                        dest='seqs', help='The filepath for the species subsequence')
    parser.add_option('-m', '--mod', action='store',
                        dest='model', help='The modeltype, \'jc\' or \'fix\' or \'hky\'')

    (args, _) = parser.parse_args() # Extract all the arguments from the parsed options
    # Stop if any required options are not specified
    if args.pscm == None or args.seqs == None or args.model == None or args.format == None:
        print("Missing required arguments")
        sys.exit(1)

    # Open the file to get the PWM text
    with open(args.pscm, 'r') as f_open:
        motif_matrix = f_open.read()

    motif_matrix = convert_matrix(motif_matrix, args.format) # Prase the text and convert into PFM format
    pseudo_count = 1 # Default value is set to 1
    motif_matrix = add_pseudo_count(motif_matrix, pseudo_count) # Add the pseudo count to the PFM
    motif_matrix = pfm_to_pwm(motif_matrix) # Convert the PFM to PWM as a standard input

    # Open the sequence file and read the content
    with open(args.seqs, 'r') as f_open:
        seqs = f_open.read()
        seqs = get_formatted_seqs(seqs) # Pre-process the sequences into standard input format

    # print(seqs)
    # print(neg_seqs)
    # print(motif_matrix)
    # print(args.model)

    # Call relevant functions based on the desired scoring techniques
    scale_lambda = 0.1
    branch_length = 0.90091
    hky_p = 0.07
    hky_q = 0.03
    model = args.model

    get_evoscore(seqs, motif_matrix, scale_lambda, branch_length, hky_p, hky_q, model)

if __name__ == "__main__":
    calScore(sys.argv[1:])

