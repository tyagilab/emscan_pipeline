import uuid
from flask import Flask, request, render_template, redirect, url_for
import os
import sys
import subprocess

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

app = Flask(__name__)

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/upload', methods=['POST'])
def upload():
    try:
        if request.method == 'POST':
            # Get the form data
            motif_file = request.files['motif_file']
            seq_file = request.files['seq_file']
            format = request.form['format']
            model = request.form['model']

            # Save the uploaded files
            motif_path = os.path.join('uploads', motif_file.filename)
            seq_path = os.path.join('uploads', seq_file.filename)
            motif_file.save(motif_path)
            seq_file.save(seq_path)

            # Run the script with the provided inputs
            result = subprocess.run([sys.executable, 'calscore.py', '-p', motif_path, '-f', format, '-s', seq_path, '-m', model], capture_output=True, text=True)

            # Generate a plot based on the results
            scores = [float(line.split()[1]) for line in result.stdout.splitlines() if line]
            positions = list(range(len(scores)))

            plot_dir = 'static/plots'
            os.makedirs(plot_dir, exist_ok=True)

            line_plot_path = os.path.join(plot_dir, 'line_plot.png')
            bar_chart_path = os.path.join(plot_dir, 'bar_chart.png')
            histogram_path = os.path.join(plot_dir, 'histogram.png')
            scatter_plot_path = os.path.join(plot_dir, 'scatter_plot.png')

            create_line_plot(positions, scores, line_plot_path)
            create_bar_chart(positions, scores, bar_chart_path)
            create_histogram(scores, histogram_path)
            create_scatter_plot(positions, scores, scatter_plot_path)

            # Return the result to the user
            return render_template('result.html', output=result.stdout,
                                line_plot_url=line_plot_path,
                                bar_chart_url=bar_chart_path,
                                histogram_url=histogram_path,
                                scatter_plot_url=scatter_plot_path)

    except Exception as e:
        return render_template('error.html', error_code=500, error_message=str(e))

    return redirect(url_for('home'))

@app.errorhandler(404)
def page_not_found(e):
    return render_template('error.html', error_code=404, error_message="Page Not Found"), 404

@app.errorhandler(500)
def internal_server_error(e):
    return render_template('error.html', error_code=500, error_message="Internal Server Error"), 500

@app.errorhandler(Exception)
def handle_exception(e):
    return render_template('error.html', error_code=500, error_message=str(e)), 500

def create_line_plot(positions, scores, plot_path):
    plt.figure()
    plt.plot(positions, scores, marker='o', label='Motif Score')
    plt.xlabel('Position')
    plt.ylabel('Score')
    plt.title('Motif Scores Across Sequences')
    plt.legend(loc='best')
    plt.savefig(plot_path)
    plt.close()

def create_bar_chart(positions, scores, plot_path):
    plt.figure()
    plt.bar(positions, scores, label='Motif Score')
    plt.xlabel('Position')
    plt.ylabel('Score')
    plt.title('Motif Scores Across Sequences')
    plt.legend(loc='best')
    plt.savefig(plot_path)
    plt.close()

def create_histogram(scores, plot_path):
    plt.figure()
    plt.hist(scores, bins=10, edgecolor='black', label='Motif Score Distribution')
    plt.xlabel('Score')
    plt.ylabel('Frequency')
    plt.title('Distribution of Motif Scores')
    plt.legend(loc='best')
    plt.savefig(plot_path)
    plt.close()

def create_scatter_plot(positions, scores, plot_path):
    plt.figure()
    plt.scatter(positions, scores, label='Motif Score')
    plt.xlabel('Position')
    plt.ylabel('Score')
    plt.title('Motif Scores Across Sequences')
    plt.legend(loc='best')
    plt.savefig(plot_path)
    plt.close()

if __name__ == '__main__':
    app.run(debug=True)