import math
# import optparse

# For computing non-evolutionary alignment probabilities.
def prob_b_given_a_none():
    return f_b

def prob_b_given_a_fixation(a, b, f_b, mu):
    """
    Return Fixation probablity
    """

    if a == b:
        return (mu * f_b) + (1- mu)
    else:
        return (mu * f_b)


def prob_b_given_a_JK(a, b, scale_lambda, branch_length):
    """
    Return JK probablity
    """

    if a == b:
        return (1/4) + ((3/4) * math.exp((4/3) * -(scale_lambda * branch_length)))
    else:
        return (1/4) - ((1/4) * math.exp((4/3) * -(scale_lambda * branch_length)))

def prob_b_given_a_HKY(a, b, motif_matrix, pos, branch_length, K):
    """
    Return HKY probablity
    """

    base_pos = {'a': 0, 'c': 1, 'g': 2, 't':3} # Letter mappings to access the PWM values from the motif

    f_a = float(motif_matrix[pos + 1][base_pos['a']])
    f_c = float(motif_matrix[pos + 1][base_pos['c']])
    f_g = float(motif_matrix[pos + 1][base_pos['g']])
    f_t = float(motif_matrix[pos + 1][base_pos['t']])
    beta = 1 / ((2 * (f_a + f_g) * (f_c + f_t)) + (2 * K * (f_a * f_g + f_c * f_t)))

    if (b in ['a', 'g'] and a in ['c', 't']) or (b in ['c', 't'] and a in ['a', 'g']):
        return(hky_transversion(float(motif_matrix[pos + 1][base_pos[b]]), beta, branch_length))
    else:
        return(hky_transition(a, b, f_a, f_c, f_g, f_t, beta, K, branch_length))


def hky_transition(a, b, f_a, f_c, f_g, f_t, beta, K, branch_length):
    base_pos = {'a': 0, 'c': 1, 'g': 2, 't':3}
    if a == 'a' and a == b:
        term1 = f_a * (f_a + f_g + (f_c + f_t) * math.exp(-beta * branch_length))
        term2 = f_g * math.exp(-(1 + (f_a + f_g) * (K - 1.0)) * beta * branch_length)
        term3 = (f_a + f_g)
        return (term1 + term2)/ term3
    elif a == 'a' and a !=b:
        term1 = f_g * (f_a + f_g + (f_c + f_t) * math.exp(-beta * branch_length))
        term2 = f_g * math.exp(-(1 + (f_a + f_g) * (K - 1.0)) * beta * branch_length)
        term3 = (f_a + f_g)
        return (term1 - term2)/ term3
    elif a == 'g' and a == b:
        term1 = f_g * (f_a + f_g + (f_c + f_t) * math.exp(-beta * branch_length))
        term2 = f_a * math.exp(-(1 + (f_a + f_g) * (K - 1.0)) * beta * branch_length)
        term3 = (f_a + f_g)
        return (term1 + term2)/ term3
    elif a == 'g' and a != b:
        term1 = f_a * (f_a + f_g + (f_c + f_t) * math.exp(-beta * branch_length))
        term2 = f_a * math.exp(-(1 + (f_a + f_g) * (K - 1.0)) * beta * branch_length)
        term3 = (f_a + f_g)
        return (term1 - term2)/ term3
    elif a == 'c' and a == b:
        term1 = f_c * (f_c + f_t + (f_a + f_g) * math.exp(-beta * branch_length))
        term2 = f_t * math.exp(-(1 + (f_c + f_t) * (K - 1.0)) * beta * branch_length)
        term3 = (f_c + f_t)
        return (term1 + term2)/ term3
    elif a == 'c' and a != b:
        term1 = f_t * (f_c + f_t + (f_a + f_g) * math.exp(-beta * branch_length))
        term2 = f_t * math.exp(-(1 + (f_c + f_t) * (K - 1.0)) * beta * branch_length)
        term3 = (f_c + f_t)
        return (term1 - term2)/ term3
    elif a == 't' and a == b:
        term1 = f_t * (f_c + f_t + (f_a + f_g) * math.exp(-beta * branch_length))
        term2 = f_c * math.exp(-(1 + (f_c + f_t) * (K - 1.0)) * beta * branch_length)
        term3 = (f_c + f_t)
        return (term1 + term2)/ term3
    elif a == 't' and a != b:
        term1 = f_c * (f_c + f_t + (f_a + f_g) * math.exp(-beta * branch_length))
        term2 = f_c * math.exp(-(1 + (f_c + f_t) * (K - 1.0)) * beta * branch_length)
        term3 = (f_c + f_t)
        return (term1 - term2)/ term3

def hky_transversion(f_b, beta, branch_length):

    return(f_b * (1.0 - math.exp(-beta * branch_length)))

# def get_subst_model():
#     if subst_model_name == 'None':
#         subst_model = prob_b_given_a_none()
#     elif subst_model_name == 'JC':
#         subst_model = prob_b_given_a_JK()
#     elif subst_model_name == 'Fix':
#         subst_model = prob_b_given_a_fixation()
#     else:
#         print('Substitution model ', subst_model_name, ' not recognized.')
#         exit(1)
#
#     return subst_model
